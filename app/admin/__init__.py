from flask import abort, redirect, url_for, request
from flask.ext.admin.contrib.sqla import ModelView
from flask.ext.security import current_user

# Create customized model view class
class MyModelView(ModelView):

    def is_accessible(self):
        if not current_user.is_active() or not current_user.is_authenticated():
            return False

        if current_user.has_role('admin'):
            return True

        return False

    def _handle_view(self, name, **kwargs):
        """
        Override builtin _handle_view in order to redirect users when a view is not accessible.
        """
        if not self.is_accessible():
            if current_user.is_authenticated():
                # permission denied
                abort(403)
            else:
                # login
                return redirect(url_for('security.login', next=request.url))


def init_admin(app, db):
    from flask.ext.admin import Admin

    from ..auth.models import User
    from ..carpool.models import Commute

    admin = Admin(app, name="rideshare", template_mode='bootstrap3')
    admin.add_view(MyModelView(User, db.session))
    admin.add_view(MyModelView(Commute, db.session))

