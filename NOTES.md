# Deployment

Install `supervisor` for scheduling long running processes.

    apt-get install supervisor


# Convert PNG to ICO

    > icotool -c static/img/logo.png -o static/img/favicon.ico    