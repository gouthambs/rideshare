from flask_security.forms import RegisterForm, Required, ConfirmRegisterForm
from wtforms import StringField, RadioField


class ExtendedRegisterForm(RegisterForm):
    name = StringField('Name',validators=[Required()])
    gender = RadioField("Gender", choices=[("M", "Male"), ("F", "Female"), ("O", "Other")],
                        validators=[Required()])


class ExtendedConfirmRegisterForm(ConfirmRegisterForm):
    name = StringField('Name',validators=[Required()])
    gender = RadioField("Gender", choices=[("M", "Male"), ("F", "Female"), ("O", "Other")],
                        validators=[Required()])