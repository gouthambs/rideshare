from test.local import BaseTestCase
from flask import url_for


class ProfileTest(BaseTestCase):

    def setUp(self):
        BaseTestCase.setUp(self)
        self._create_test_user()
        self._logout_user()

    def tearDown(self):
        BaseTestCase.tearDown(self)

    def test_profile(self):
        with self.app.test_request_context():
            self._login_test_user()
            profile_url = url_for("carpool.profile")
            response = self.client.get(profile_url)
            print response.data
