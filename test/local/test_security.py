from test.local import BaseTestCase
from flask_security import url_for_security
import copy

class SecurityTest(BaseTestCase):
    """
    Tests the registration process to see if we are
    able to register, login, logout etc.
    """

    def test_index(self):
        """
        Check we can access the index page
        :return:
        """
        with self.client:
            response = self.client.get("/")
            self.assertEqual(response.status_code, 200)

    def test_register(self):
        """
        Check users can register
        :return:
        """
        with self.app.test_request_context():
            register_url = url_for_security("register")
            response = self.client.get(register_url)
            self.assertEqual(response.status_code, 200)
            response = self._create_test_user()

            self.assertEqual(response.status_code, 302)
            self.assertTrue('<a href="/">/</a>' in response.data)

            # now check in user and user_profile table
            from app.auth.models import User
            from app.carpool.models import UserProfile
            user = User.query.filter_by(email="testuser@testland.com").first()
            self.assertTrue(user is not None)
            user_profile = UserProfile.query.filter_by(user_id=user.id).first()
            self.assertTrue(user_profile is not None)

    def test_register_uniqueness(self):
        """
        Signing up with same email should be forbidden
        :return:
        """
        with self.app.test_request_context():
            response = self._create_test_user()
            print response.data
            self._logout_user()

            response = self._create_test_user()
            self.assertTrue("<li>testuser@testland.com is already associated "
                            "with an account.</li>" in response.data)

    def test_login(self):
        """
        Test users have the functionality to login
        :return:
        """
        self._create_test_user()
        self._logout_user()
        with self.app.test_request_context():
            login_url = url_for_security("login")

            response = self.client.get(login_url)
            self.assertEqual(response.status_code, 200)

            response = self._login_test_user()
            self.assertEqual(response.status_code, 302)

            self.assertTrue('<a href="/home/">/home/</a>' in response.data)

    def test_change_password(self):
        """
        Test users can change password
        :return:
        """
        self._create_test_user()
        with self.app.test_request_context():
            change_url = url_for_security("change_password")
            response = self.client.get(change_url)
            self.assertEqual(response.status_code, 200)

            data = {"password": "testpassword","new_password": "newpassword",
                    "new_password_confirm": "newpassword"}
            response = self.client.post(change_url, data=data)
            self.assertEqual(response.status_code, 302)
            self.assertTrue('You should be redirected automatically to target '
                            'URL: <a href="/home/">/home/</a>' in response.data)
            self._logout_user()

            # old password should fail now
            response = self._login_test_user()
            self.assertTrue("<li>Invalid password</li>" in response.data)
            # new password should work
            response = self._login_test_user({"email": self.registration_data["email"],
                                              "password": "newpassword"})
            self.assertTrue('You should be redirected automatically to target URL: '
                            '<a href="/home/">/home/</a>' in response.data)