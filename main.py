import os
import sys
from app import create_app



if len(sys.argv) < 2:
    print "Usage: main.py <config_file>"
    exit(1)
#BASE_DIR = os.path.dirname(__file__)
#config_file = os.path.abspath(sys.argv[1])
config_file = sys.argv[1]
app = create_app(config_file)

if __name__ == '__main__':
    app.run(debug=True)
