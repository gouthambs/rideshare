from flask import Blueprint, render_template, current_app, redirect, url_for, \
    request, after_this_request
from auth.forms import ExtendedConfirmRegisterForm, ExtendedRegisterForm
from flask.ext.security import current_user
from forms import CommuteSearchForm
from models import CommuteSearch
import markdown
import os
from .carpool.views import get_lat_lng_box
from .persistence import get_db, get_cache
import logging
import datetime
import base64
from htmlmin.main import minify

main = Blueprint("main", __name__)
db = get_db()
cache = get_cache()
_logger = logging.getLogger("rideshare")


def _save_and_fetch_searches(search_form):
    distance = 15*1609
    search = CommuteSearch(
        start_area=search_form.start_locality.data+", "+search_form.start_state.data,
        end_area=search_form.end_locality.data+", "+search_form.end_state.data,
        start_address=search_form.start_address.data,
        end_address=search_form.end_address.data,
        start_latitude=search_form.start_latitude.data,
        start_longitude=search_form.start_longitude.data,
        end_latitude=search_form.end_latitude.data,
        end_longitude=search_form.end_longitude.data,
        datetime=datetime.datetime.utcnow()
    )

    start_lat_rng, start_lng_rng = get_lat_lng_box(search.start_latitude,
                                                   search.start_longitude,
                                                   distance)
    end_lat_rng, end_lng_rng = get_lat_lng_box(search.end_latitude,
                                               search.end_longitude,
                                               distance)

    result = CommuteSearch.query.filter(CommuteSearch.start_latitude.between(*start_lat_rng),
                                    CommuteSearch.start_longitude.between(*start_lng_rng),
                                    CommuteSearch.end_latitude.between(*end_lat_rng),
                                    CommuteSearch.end_longitude.between(*end_lng_rng)).limit(10).all()

    self_location = [{"start": [search.start_latitude, search.start_longitude],
                      "end": [search.end_latitude, search.end_longitude],
                      "content": "Your search",
                      "type": "self"
                      }]
    locations = self_location + [{"start": [c.start_latitude, c.start_longitude],
                                  "end": [c.end_latitude, c.end_longitude],
                                  'content': str("%s to %s" % (c.start_area, c.end_area)),
                                  'type': "other"} for i, c in enumerate(result)]

    try:
        db.session.add(search)
        db.session.commit()
    except Exception as e:
        _logger.exception(str(e))
    return result, locations


@cache.memoize(timeout=500)
def _render_index(is_authenticated):
    security = current_app.extensions["security"]
    search_form = CommuteSearchForm()
    if security.confirmable:
        register_user_form = ExtendedConfirmRegisterForm()
    else:
        register_user_form = ExtendedRegisterForm()
    #blogging = current_app.extensions["FLASK_BLOGGING_ENGINE"]
    #posts = blogging.storage.get_posts(count=5, recent=True)

    img_data = None
    file_name = os.path.join(current_app.static_folder, "img", "city_street_sm2.jpg" )
    with open(file_name, "rb") as imageFile:
        img_data = base64.b64encode(imageFile.read())

    html = render_template("index.html",
                           register_user_form=register_user_form,
                           search_form=search_form,
                           img_data=img_data)
    html_min = minify(html)
    return html_min


@main.route("/", methods=["GET"])
def index():
    view = _render_index(current_user.is_authenticated())
    return view


@main.route("/peek/", methods=["GET", "POST"])
def search():
    search_form = CommuteSearchForm()
    search_result=None
    locations=None
    if request.method == "POST":
        if search_form.validate_on_submit():
            search_result, locations = _save_and_fetch_searches(search_form)

    return render_template("search.html",
                           search_form=search_form,
                           search_result=search_result,
                           locations=locations)


@cache.cached(timeout=60)
def _render_tou():
    md = markdown.Markdown()
    f = open(os.path.join(current_app.template_folder, "terms_of_use.md"))
    text = f.read()
    f.close()
    content = md.convert(text)
    return content


@main.route("/tou/")
def terms_of_use():
    content = _render_tou()
    return render_template("terms_of_use.html", content=content)