from redis import Redis
import smtplib


def configure_smtp_host(conf):
    if conf.get("MAIL_USE_SSL", False):
        host = smtplib.SMTP_SSL(conf.get("MAIL_SERVER"), conf.get("MAIL_PORT"))
    else:
        host = smtplib.SMTP(conf.get("MAIL_SERVER"), conf.get("MAIL_PORT"))

    if conf.get("MAIL_USE_TLS"):
        host.starttls()
    if conf["MAIL_USERNAME"] and conf["MAIL_PASSWORD"]:
        host.login(conf["MAIL_USERNAME"], conf["MAIL_PASSWORD"])
    return host


def send_email(msg):
    from flask import current_app as app
    mail = app.extensions["mail"]
    with app.app_context():
        mail.send(msg)


def delay_send_email(msg):
    from flask import current_app as app
    q = app.extensions.get("q")
    if q:
        q.enqueue(send_email, msg)
    else:
        send_email(msg)


def init_task_queue(app):
    from rq import Queue
    q = Queue(connection=Redis())
    app.extensions["q"] = q

    security = app.extensions["security"]
    security.send_mail_task(delay_send_email)





