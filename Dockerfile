# Dockerfile for deploying the app

FROM ubuntu:latest


# install some dependencies
# Install required packages
RUN \
  apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    supervisor \
    python-dev \
    python \
    python-pip \
    ca-certificates



RUN rm -rf /var/lib/apt/lists/*


# Create directories
RUN mkdir -p /var/www/app  /var/www/app-conf /var/log/supervisor
WORKDIR /var/www/app
ADD . /var/www/app
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf



# install app dependencies
RUN pip install -r /var/www/app/Requirements.txt
RUN pip install uwsgi==2.0.11


EXPOSE 8081

CMD ["/usr/bin/supervisord"]



