from flask.ext.wtf import Form
from wtforms.fields import StringField, SubmitField, FloatField
from wtforms.validators import DataRequired


class CommuteSearchForm(Form):
    start_address = StringField("Start Address", validators=[DataRequired()])
    end_address = StringField("End Address", validators=[DataRequired()])
    start_locality = StringField("Start Locality")
    end_locality = StringField("End Locality")
    start_state = StringField("Start State")
    end_state = StringField("End State")
    start_latitude = FloatField("Start Latitude")
    start_longitude = FloatField("Start Longitude")
    end_latitude = FloatField("End Latitude")
    end_longitude = FloatField("End Longitude")
    submit = SubmitField("Search")
