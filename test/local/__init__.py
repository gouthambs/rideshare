import unittest
from app import create_app
import os
from app.persistence import get_db
from flask_security import url_for_security
import tempfile


class BaseTestCase(unittest.TestCase):

    registration_data = {
        "name": "Test User",
        "email": "testuser@testland.com",
        "gender": "F",
        "password": "testpassword",
        "password_confirm": "testpassword",
    }

    def _create_test_user(self, registration_data=None):
        with self.app.test_request_context():
            register_url = url_for_security("register")
            registration_data = registration_data or self.registration_data
            response = self.client.post(register_url, data=registration_data)
        return response

    def _login_test_user(self, login_data=None):
        with self.app.test_request_context():
            login_url = url_for_security("login")
            login_data = login_data or self.registration_data
            response = self.client.post(login_url, data=login_data)
        return response

    def _logout_user(self):
        with self.app.test_request_context():
            logout_url = url_for_security("logout")
            response = self.client.get(logout_url)
        return response

    def _get_title(self, name):
        return "<title>%s - %s</title>" % (self.app.config["SITENAME"], name)

    @classmethod
    def setUpClass(cls):
        config_file = os.path.join(os.path.dirname(__file__), "test.cfg")
        cls.app = create_app(config_filename=config_file)
        #cls.temp_db = cls.app.config["DB_FILE"]
        #cls.app.config["SQLALCHEMY_DATABASE_URI"] ="sqlite:///" + cls.temp_db
        cls.client = cls.app.test_client()
        with cls.app.app_context():
            db = get_db()
            db.create_all()

    def tearDown(self):
        with self.app.app_context():
            db = get_db()
            meta = db.metadata
            for tbl in reversed(meta.sorted_tables):
                db.engine.execute(tbl.delete())

    @classmethod
    def tearDownClass(cls):
        #os.remove(cls.temp_db)
        pass
