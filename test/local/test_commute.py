from test.local import BaseTestCase
from flask import url_for
import datetime
import copy


class CommuteTest(BaseTestCase):
    commute_data = {'comments': u'',
                    'commute_days': [1, 2, 4, 8, 16],
                    'end_address': u'Santa Monica, CA, United States',
                    'end_latitude': 34.0195,
                    'end_longitude': -118.491,
                    'end_time': datetime.time(15, 30),
                    'share_type': [2],
                    'start_address': u'Cerritos, CA, United States',
                    'start_latitude': 33.8583,
                    'start_longitude': -118.065,
                    'start_time': datetime.time(7, 30)}

    def setUp(self):
        BaseTestCase.setUp(self)
        self._create_test_user()
        self._logout_user()

    def tearDown(self):
        BaseTestCase.tearDown(self)

    def test_home(self):
        with self.app.test_request_context():
            # unauthenticated user can't access
            home_url = url_for("carpool.index")
            response = self.client.get(home_url)
            self.assertEqual(response.status_code, 302)
            self.assertEqual(response.location, "http://localhost/login?next=%2Fhome%2F")

            # authenticated user can access
            self._login_test_user()
            response = self.client.get(home_url)
            self.assertEqual(response.status_code, 200)
            self.assertTrue(self._get_title("Home") in response.data)

    def test_add_commute(self):
        """
        Test we can add commute
        :return:
        """
        from app.carpool.models import Commute
        with self.app.test_request_context():
            # unauthenticated user can't create commute
            commute_url = url_for("carpool.edit_commute")
            response = self.client.get(commute_url)
            self.assertEqual(response.status_code, 302)
            self.assertEqual(response.location, "http://localhost/login?next=%2Fcommute%2Fedit%2F")

            # authenticated user can access
            self._login_test_user()
            response = self.client.post(url_for("carpool.edit_commute"), data=self.commute_data)
            self.assertEqual(response.status_code, 302)
            self.assertEqual(response.location, 'http://localhost/home/')

            # check commute data exists, and the values are the same
            commute_count = Commute.query.filter().count()
            self.assertEqual(commute_count, 1)
            commute = Commute.query.filter().first()
            for k, v in self.commute_data.iteritems():
                value = getattr(commute, k)
                if k in set(["share_type", "commute_days"]):
                    self.assertEqual(value, sum(v))
                else:
                    self.assertEqual(value, v)

    def test_edit_commute(self):
        """
        Test we can edit a previously entered commute.
        :return:
        """
        from app.carpool.models import Commute
        with self.app.test_request_context():

            # authenticated user can access
            self._login_test_user()
            response = self.client.post(url_for("carpool.edit_commute"), data=self.commute_data)
            self.assertEqual(response.status_code, 302)
            self.assertEqual(response.location, 'http://localhost/home/')

            commute = Commute.query.filter().first()

            modified_data = copy.deepcopy(self.commute_data)
            modified_data["comments"] = "Test Comment"
            response = self.client.post(url_for("carpool.edit_commute", commute_id=commute.id),
                                        data=modified_data)
            self.assertEqual(response.status_code, 302)
            self.assertEqual(response.location, 'http://localhost/home/')

            commute_count = Commute.query.filter().count()
            self.assertEqual(commute_count, 1)
            commute = Commute.query.filter().first()

            for k, v in modified_data.iteritems():
                value = getattr(commute, k)
                if k in set(["share_type", "commute_days"]):
                    self.assertEqual(value, sum(v))
                else:
                    self.assertEqual(value, v)

    def test_add_commute_over_max(self):
        """
        Users will not be allowed to enter more than one commute.
        (There is MAX_COMMUTES config variable to override this)
        :return:
        """

        from app.carpool.models import Commute
        with self.app.test_request_context():
            self._login_test_user()
            # make one entry
            response = self.client.post(url_for("carpool.edit_commute"), data=self.commute_data)
            self.assertEqual(response.status_code, 302)
            self.assertEqual(response.location, 'http://localhost/home/')

            # now even a get request should redirect
            response = self.client.get(url_for("carpool.edit_commute"))
            self.assertEqual(response.status_code, 302)
            self.assertEqual(response.location, 'http://localhost/home/')

            # now a post request would reroute
            response = self.client.post(url_for("carpool.edit_commute"), data=self.commute_data)
            self.assertEqual(response.status_code, 302)
            self.assertEqual(response.location, 'http://localhost/home/')

            commute_count = Commute.query.filter().count()
            self.assertEqual(commute_count, 1)

    def test_user_edit_commute_security(self):
        """
        A user should not have access to edit other user's commute
        :return:
        """
        from app.carpool.models import Commute
        from app.auth.models import User
        with self.app.test_request_context():
            modified_data = copy.deepcopy(self.commute_data)
            modified_data["comments"] = "Test Comment"

            # login default user, and register commute
            self._login_test_user()
            response = self.client.post(url_for("carpool.edit_commute"), data=self.commute_data)
            self._logout_user()

            # create another user, and register commute
            other_user_data = copy.deepcopy(self.registration_data)
            other_user_data["email"] = "otheruser@testland.com"
            self._create_test_user(other_user_data)
            response = self.client.post(url_for("carpool.edit_commute"), data=self.commute_data)
            self._logout_user()

            # try to edit the other person's commute and see it is disallowed
            test_user = User.query.filter_by(email=self.registration_data["email"]).first()
            other_user = User.query.filter_by(email=other_user_data["email"]).first()
            test_user_commute = Commute.query.filter_by(user_id=test_user.id).first()
            other_user_commute = Commute.query.filter_by(user_id=other_user.id).first()

            # test user tries to edit other user's commute
            self._login_test_user()
            response = self.client.post(url_for("carpool.edit_commute", commute_id=other_user_commute.id),
                                        data=modified_data, follow_redirects=True)

            self.assertTrue("You do not have access to this page!" in response.data)
            self._logout_user()

            self._login_test_user(other_user_data)
            response = self.client.post(url_for("carpool.edit_commute", commute_id=test_user_commute.id),
                                        data=modified_data, follow_redirects=True)

            self.assertTrue("You do not have access to this page!" in response.data)
            self._logout_user()

    def test_search_commute_test(self):
        """
        Test search results show commutes
        :return:
        """
        from app.carpool.models import Commute
        from app.auth.models import User
        with self.app.test_request_context():
            # login default user, and register commute
            self._login_test_user()
            response = self.client.post(url_for("carpool.edit_commute"), data=self.commute_data)
            self._logout_user()

            # create another user, and register commute
            other_user_data = copy.deepcopy(self.registration_data)
            other_user_data["email"] = "otheruser@testland.com"
            other_user_data["name"] = "Other User"
            self._create_test_user(other_user_data)
            response = self.client.post(url_for("carpool.edit_commute"), data=self.commute_data)
            self._logout_user()

            test_user = User.query.filter_by(email=self.registration_data["email"]).first()
            other_user = User.query.filter_by(email=other_user_data["email"]).first()
            test_user_commute = Commute.query.filter_by(user_id=test_user.id).first()
            other_user_commute = Commute.query.filter_by(user_id=other_user.id).first()

            # now test user should find other user in the search
            self._login_test_user()
            search_url = url_for("carpool.search_commute", commute_id=test_user_commute.id)
            response = self.client.get(search_url)
            self.assertEqual(response.status_code, 200)

            search_url = url_for("carpool.search_commute", commute_id=test_user_commute.id,
                                 start_miles=5.0, end_miles=5.0)
            response = self.client.get(search_url)
            self.assertEqual(response.status_code, 200)
            self.assertTrue('<a href="/profile/2/"> Other User</a>' in response.data)




