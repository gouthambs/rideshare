

def send_email(msg):
    # Use the Flask-Mail extension instance to send the
    # incoming ``msg`` parameter which is an instance of
    # `flask_mail.Message`
    from flask import current_app as app
    mail = app.extensions["mail"]
    with app.app_context():
        mail.send(msg)
