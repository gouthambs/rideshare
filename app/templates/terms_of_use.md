# Terms of Use

_These terms and conditions of use were last updated on August 7, 2015_


**Introduction** 

Thank you for using ucarpool.org (the "Website"). This page contains  the 
terms that apply to your use of the Website (the "Terms"). By using the 
Website, you are agreeing to be legally bound by the Terms. We may change
the Terms from time to time without notice, so be sure to check this page 
regularly. The date of the last revision or update appears at the top under 
the title. By using the Website, you are agreeing to these Terms of use. 
If you do not agree with these Terms of use, then do not use the site.
You use of the Website after such changes constitutes acceptance of the 
changes.

**Site Information**

This Website uses standard internet technology and practices to ensure
availability and reliability, and to protect the security of the information
provided by you to find carpool matches for you. However, disruptions
are always possible as a result of accidents or intentional interference. 
In no event are the Website, or its operators liable for any injuries
or losses by any party related to the use of this Website. By using
the services offered by this Website, you agree to assume all of the risk
when using the services described herein including, but not limited to,
all of the risks associated with any online or offline interactions
with other users. You agree to read the safety section below 
which offers safety suggestions.


**Eligibility**

- You are at least 18 years of age
- You have not committed any felony or serious criminal offense (including
  but not limited to a sexual offense and/or a violence-related offense) ever
  or any misdemeanor within the past three (3) years
  
**Safety**

This Website provides a means for you to contact and meet new people
who may have similar commuting needs. This Website does not ensure
lawful behavior of the people exchanging information through its 
website. You take full responsibility for ensuring your personal 
safety when interacting with other people whom you meet through 
this Website. 


**Privacy Notice**

UCarPool.org respects the privacy of users of this Website. By visiting
this site, you are agreeing to the terms of this privacy statement. If you 
are not happy with the terms of this privacy statement and do not wish 
your information be recorded, you may discontinue using this Website.
 
UCarPool.org respects the rights of its users. The various personal information 
collected through this website is listed below:

1. Information provided voluntarily by the user while searching for a carpool match
2. While registering on the Website, you will be asked to provide name, email address,
and gender. 
3. While registering your commute, you will be asked to provide start address, and end address,
the days of commute, and commute start and end times.
4. Statistical information gathered when you visit our website.

The visitor web statistics are recorded through Google Analytics. By visiting
this site, you are agreeing to the terms of use of Google Analytics.


** Use of Data **

UCarPool.org uses your email address as a means to verify the registration process, 
to contact you for providing ride matches, and to reach out for product feedback.
You email address will not be revealed to other users participating in the Website.

The Website uses your commute information to match other users with similar commutes.
Your addresses will not be revealed to other users participating in the Website.

The visitor web statistics collected through Google Analytics is used for
understanding user traffic and for future marketing and growth.


**Indemnification**

You agree to defend, indemnify and hold harmless this Website, its officers, 
directors, employees, owners, agents and third parties for any losses, costs, 
liabilities, expenses and damages or against any claims by third parties, 
including reasonable attorneys fees relating to or arising out of your 
use of this site, its services, your violation of this Agreement or your violation 
of applicable laws so long as they do not result from any negligence on the 
part of UCarPool.org.

** Additional Information**

For additional information, contact info[dot]ucarpool[at]gmail[dot]com.