from persistence import get_db

db = get_db()


class CommuteSearch(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    start_area = db.Column(db.String(50))
    end_area = db.Column(db.String(50))
    start_address = db.Column(db.String(255))
    end_address = db.Column(db.String(255))
    start_latitude = db.Column(db.Float(), index=True)
    start_longitude = db.Column(db.Float(), index=True)
    end_latitude = db.Column(db.Float(), index=True)
    end_longitude = db.Column(db.Float(), index=True)
    datetime = db.Column(db.DateTime())
