from flask.ext.wtf import Form
from wtforms import StringField, Field, SelectMultipleField, SubmitField, TextAreaField, \
    FloatField, IntegerField
from wtforms.widgets import TextInput, ListWidget, CheckboxInput
from wtforms.validators import DataRequired, Email
import datetime


class TimeField(Field):
    widget = TextInput()

    def _value(self):
        if self.data:
            return u'%d:%d' % (self.data.hour, self.data.minute)
        else:
            return u""

    def process_formdata(self, valuelist):
        if valuelist:
            time_str = ' '.join(valuelist)
            try:
                value = time_str.split(":")
                self.data = datetime.time(int(value[0]), int(value[1]))
            except ValueError:
                self.data = None
                raise ValueError(self.gettext('Not a valid time value'))

class MultiCheckboxField(SelectMultipleField):
    widget = ListWidget(prefix_label=False)
    option_widget = CheckboxInput()


class CommuteForm(Form):
    start_address = StringField("From", validators=[DataRequired()])
    end_address = StringField("To", validators=[DataRequired()])
    start_latitude = StringField("Start Latitude", validators=[DataRequired()])
    start_longitude = StringField("Start Longitude", validators=[DataRequired()])
    end_latitude = StringField("End Latitude", validators=[DataRequired()])
    end_longitude = StringField("End Longitude", validators=[DataRequired()])
    start_time = TimeField("Start Time", validators=[DataRequired()], default=datetime.time(7, 30))
    end_time = TimeField("End Time", validators=[DataRequired()], default=datetime.time(16,30))
    day_choices = [(1, "M"), (2, "T"), (4, "W"), (8, "T"), (16, "F"), (32, "S"), (64, "S")]
    commute_days = MultiCheckboxField('Days',
                                      coerce=int,
                                      choices=day_choices,
                                      default=[1, 2, 4, 8, 16])
    share_type = MultiCheckboxField("Interest",
                                    coerce=int,
                                    default=[2],
                                    choices=[(1, "Drive Alone"), (2, "Share Drive"),
                                             (4, "Share Cost")],
                                    validators=[DataRequired()])
    comments = TextAreaField("Comments")
    start_locality = StringField("Start Locality")
    end_locality = StringField("End Locality")
    start_state = StringField("Start State")
    end_state = StringField("End State")
    submit = SubmitField("Submit")


class CommuteSearchForm(Form):
    start_distance = FloatField("Start Distance", default=5.0)
    end_distance = FloatField("End Distance", default=5.0)
    submit = SubmitField("Search")


class UserProfileForm(Form):
    name = StringField("Name", validators=[DataRequired()])
    description = TextAreaField("About Me")
    email = StringField("Email")
    submit = SubmitField("Submit")
    #start_flexibility = StringField("Start Time Flexibility", default=10, coerce=int, validators=[DataRequired()])


class MessageForm(Form):
    subject =  StringField("Subject", validators=[DataRequired()])
    body = TextAreaField("Message", validators=[DataRequired()])
    sender_id = IntegerField("Sender Id")
    receiver_id = IntegerField("Receiver Id")
    parent_id = IntegerField("Parent Id")
    submit = SubmitField("Send")

