"""
Hook up with flask security
"""




def init_security(current_app, db):
    from flask.ext.security import SQLAlchemyUserDatastore, Security
    from .models import User, Role
    # Setup Flask-Security
    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    from forms import ExtendedRegisterForm, ExtendedConfirmRegisterForm
    security = Security(current_app, user_datastore,
                        register_form=ExtendedRegisterForm,
                        confirm_register_form=ExtendedConfirmRegisterForm
                        )
    return security



