from flask import Blueprint, render_template, request, flash, redirect, url_for, current_app
from flask.ext.security import login_required, current_user
from flask.ext.mail import Message as MailMessage
from ..auth.models import User
import hashlib
from forms import CommuteForm, UserProfileForm, CommuteSearchForm, MessageForm
from models import Commute, UserProfile, Message, LocalArea
import datetime
from ..persistence import get_db
from math import cos, pi
import random
import logging
from sqlalchemy import desc, or_
import markdown
from ..tasks import delay_send_email


db = get_db()
carpool = Blueprint("carpool", __name__)
_logger = logging.getLogger("carpool")


def get_lat_lng_box(lat, lng, distance):
    """
    Approximate calculation for the latitude longitude box
    :param lat: Latitude in degrees
    :param lng: Longitude in degrees
    :param distance: Distance in meters
    :return: [(lat1, lat2), (lng1, lng2)] the box that bounds the
    given coordinate to within given distance
    """
    R = 6371009.0  # mean radius of earth in meters
    dn = de = distance
    dLat = dn/R
    dLng = de/(R*cos(pi*lat/180))
    latp = lat + dLat * 180/pi
    lngp = lng + dLng * 180/pi
    latm = lat - dLat * 180/pi
    lngm = lng - dLng * 180/pi
    return [(latm, latp), (lngm, lngp)]


def _get_gravatar(user_email):
    hash = hashlib.md5(user_email.lower()).hexdigest()
    gravatar_url = "https://gravatar.com/avatar/%s?s=200" % hash
    return gravatar_url


def _get_or_create_local_area(locality, state):
    local_area = LocalArea.query.filter_by(locality=locality, state=state).first()
    if local_area:
        return local_area
    else:
        local_area = LocalArea(locality=locality, state=state)
        db.session.add(local_area)
        return local_area

def _store_commute_form(commute_form, commute):
    try:
        commute.start_address = commute_form.start_address.data
        commute.end_address = commute_form.end_address.data
        commute.commute_days = 127  # change this later
        commute.start_time = commute_form.start_time.data
        commute.end_time = commute_form.end_time.data
        commute.user_id = current_user.id
        commute.start_latitude = float(commute_form.start_latitude.data)
        commute.start_longitude = float(commute_form.start_longitude.data)
        commute.end_latitude = float(commute_form.end_latitude.data)
        commute.end_longitude = float(commute_form.end_longitude.data)
        commute.commute_days = sum([int(d) for d in commute_form.commute_days.data])
        commute.share_type = sum([int(d) for d in commute_form.share_type.data])
        now = datetime.datetime.utcnow()
        if commute.create_date is None:
            commute.create_date = now
        commute.last_modified_date = now
        commute.comments = commute_form.comments.data
        local_area = []
        if commute_form.start_locality.data and commute_form.start_state.data:
            start_area = _get_or_create_local_area(commute_form.start_locality.data,
                                                   commute_form.start_state.data)
            local_area.append(start_area)
        if commute_form.end_locality.data and commute_form.end_state.data:
            end_area = _get_or_create_local_area(commute_form.end_locality.data,
                                                 commute_form.end_state.data)
            local_area.append(end_area)
        commute.local_area = local_area
        db.session.add(commute)
        db.session.commit()
    except Exception as e:
        _logger.exception(str(e))


def _get_filled_commute_form(commute):
    if commute:
        cd_val = commute.commute_days
        commute_days = [i for i in [1, 2, 4, 8, 16, 32, 64] if (i & cd_val)]
        st_val = commute.share_type
        share_type = [i for i in [1, 2, 4] if (i & st_val)]
        start_locality = start_state = end_state = end_locality = None
        if commute.local_area:
            for l in commute.local_area:
                if l.locality in commute.start_address:
                    start_locality = l.locality
                    start_state = l.state
                if l.locality in commute.end_address:
                    end_locality = l.locality
                    end_state = l.state

        commute_form = CommuteForm(start_address=commute.start_address,
                                   end_address=commute.end_address,
                                   start_latitude=commute.start_latitude,
                                   start_longitude=commute.start_longitude,
                                   end_latitude=commute.end_latitude,
                                   end_longitude=commute.end_longitude,
                                   commute_days=commute_days,
                                   start_time=commute.start_time,
                                   end_time=commute.end_time,
                                   share_type=share_type,
                                   comments=commute.comments,
                                   start_locality=start_locality,
                                   end_locality=end_locality,
                                   start_state=start_state,
                                   end_state=end_state
                                   )

    else:
        commute_form = CommuteForm()
    return commute_form


def to_dict(inst, cls):
    convert = dict()
    d = dict()
    for c in cls.__table__.columns:
        v = getattr(inst, c.name)
        if c.type in convert.keys() and v is not None:
            try:
                d[c.name] = convert[c.type](v)
            except:
                d[c.name] = "Error:  Failed to covert using ", str(convert[c.type])
        elif v is None:
            d[c.name] = str()
        else:
            d[c.name] = v
    return d


def _get_user_commutes(user_id):
    commutes = Commute.query.filter_by(user_id=user_id).all()
    for c in commutes:
        c.days_str = _days_str(c.commute_days)
    return commutes


def _days_str(code):
    days = ["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"]
    commute_days = [days[i] for i, c in enumerate([1, 2, 4, 8, 16, 32, 64]) if (c & code)]
    days_str = "-".join(commute_days)
    return days_str


def _get_filled_user_profile_form(user_profile):
    if user_profile:
        user = user_profile.user
        user_profile_form = UserProfileForm(
            name=user.name,
            description=user_profile.description,
            email=user.email
        )
        return user_profile_form
    else:
        return None


def _save_user_profile_form(user_profile_form, user_id):
    try:
        user_profile = UserProfile.get(user_id)
        user = user_profile.user
        user.name = user_profile_form.name.data
        user_profile.description = user_profile_form.description.data
        db.session.add(user_profile)
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        _logger.exception(str(e))


def _store_message_form(message_form, sender_id, receiver_id, thread_id):
    now = datetime.datetime.utcnow()
    try:
        message = Message(subject=message_form.subject.data,
                          body=message_form.body.data,
                          sender_id=sender_id,
                          receiver_id=receiver_id,
                          thread_id=thread_id,
                          datetime=now
                          )
        db.session.add(message)
        db.session.commit()
    except Exception as e:
        _logger.exception(str(e))


def _inform_new_message(message_form, sender, receiver):

    msg = MailMessage('Message From UCarpool User',
                      sender=sender.email,
                      recipients=[receiver.email])

    msg.body = render_template('carpool/email/notify.txt', sender=sender,
                               receiver=receiver)
    msg.html = render_template('carpool/email/notify.html', sender=sender,
                               receiver=receiver)
    delay_send_email(msg)

    return


@carpool.context_processor
def utility_functions():
    def num_unread_messages():
        int_user_id = int(current_user.get_id())
        unread_messages = Message.query.filter_by(receiver_id=int_user_id, read_status=0).count()
        return unread_messages
    return dict(unread_messages=num_unread_messages()
                )

@carpool.route("/home/")
@login_required
def index():
    commutes = _get_user_commutes(current_user.id)
    return render_template("carpool/index.html", commutes=commutes)


def _edit_commute_get(commute_id, commute_form):
    if commute_id:  # if not None
        commute = Commute.query.filter_by(id=commute_id).first()
        if commute:
            commute_form = _get_filled_commute_form(commute)
        else:
            commute_id = None
    if commute_id is None:
        count_commutes = Commute.query.filter_by(user_id=current_user.id).count()
        if count_commutes >= current_app.config.get("MAX_COMMUTES", 1):
            return redirect(url_for("carpool.index"))
    return render_template("carpool/commute.html", commute_form=commute_form,
                           commute_id=commute_id)


def _edit_commute_post(commute_id, commute_form):
    if commute_form.validate_on_submit():
        try:
            if commute_id:  # if not None
                commute = Commute.query.filter_by(id=commute_id).first()
                if (commute.user_id != current_user.id):
                    flash("You do not have access to this page!")
                    return redirect(url_for("carpool.index"))

            if commute_id is None:
                count_commutes = Commute.query.filter_by(user_id=current_user.id).count()
                if count_commutes >= current_app.config.get("MAX_COMMUTES", 1):
                    flash("You already have entries")
                    return redirect(url_for("carpool.index"))
                commute = Commute()
            _store_commute_form(commute_form, commute)
            flash("Your commute added to the database")
            return redirect(url_for("carpool.index"))
        except Exception as e:
            flash("Errors occurred storing your commute")
            return render_template("carpool/commute.html",
                                   commute_form=commute_form)
    else:
        flash("There were errors in your submission")
        return render_template("carpool/commute.html",
                               commute_form=commute_form)


@carpool.route("/commute/edit/", methods=["GET", "POST"],
               defaults={"commute_id": None})
@carpool.route("/commute/edit/<int:commute_id>/", methods=["GET", "POST"])
@login_required
def edit_commute(commute_id):
    commute_form = CommuteForm()
    if request.method == "GET":
        return _edit_commute_get(commute_id, commute_form)
    else:  # POST
        return _edit_commute_post(commute_id, commute_form)


@carpool.route("/search/<int:commute_id>/", methods=["GET"], defaults={"start_miles": None,
                                                                       "end_miles": None})
@carpool.route("/search/<int:commute_id>/<float:start_miles>/<float:end_miles>/", methods=["GET"])
@login_required
def search_commute(commute_id, start_miles, end_miles):
    commute = Commute.query.filter_by(id=commute_id, user_id=current_user.id).first()
    commute_search_form = CommuteSearchForm()
    if commute:
        if (start_miles is None) or (end_miles is None):
            return render_template("carpool/search.html", commute=commute,
                                   commute_search_form=commute_search_form)
        else:
            # validate commute id
            commute_search_form.start_distance.data = start_miles
            commute_search_form.end_distance.data = end_miles
            start_distance = start_miles*1609  # convert distance to meters
            end_distance = end_miles*1609  # convert distance to meters



            start_lat_rng, start_lng_rng = get_lat_lng_box(commute.start_latitude,
                                                           commute.start_longitude, start_distance)
            end_lat_rng, end_lng_rng = get_lat_lng_box(commute.end_latitude, commute.end_longitude,
                                                       end_distance)

            # fetch from database
            result = Commute.query.filter(Commute.start_latitude.between(*start_lat_rng),
                                          Commute.start_longitude.between(*start_lng_rng),
                                          Commute.end_latitude.between(*end_lat_rng),
                                          Commute.end_longitude.between(*end_lng_rng),
                                          Commute.user_id != current_user.id,
                                          Commute.share_type>1
                                          ).limit(25).all()
            for r in result:
                r.user = User.get(r.user_id)
                r.days_str = _days_str(r.commute_days)
                # anonymize search results
                r.start_latitude +=  random.normalvariate(0.0, 0.005)
                r.start_longitude += random.normalvariate(0.0, 0.005)
                r.end_latitude += random.normalvariate(0.0, 0.005)
                r.end_longitude += random.normalvariate(0.0, 0.005)

            #  Commute.commute_days.op('&')(commute.commute_days)!=0
            self_location = [{"start": [commute.start_latitude, commute.start_longitude],
                              "end": [commute.end_latitude, commute.end_longitude],
                              "content": "Your commute",
                              "type": "self"
                              }]
            locations = self_location + [{"start": [c.start_latitude, c.start_longitude],
                                          "end": [c.end_latitude, c.end_longitude],
                                          'content': str("#%d %s" %(c.id, c.user.name)),
                                          'type': "other"} for i, c in enumerate(result)]


            return render_template("carpool/search.html",
                                   commute=commute,
                                   locations=locations,
                                   search_result=result,
                                   commute_search_form=commute_search_form)
    else:
        flash("Given commute does not exist or you don't have access to it.")
        return redirect(url_for("carpool.index"))


@carpool.route("/profile/", defaults={"user_id": None})
@carpool.route("/profile/<int:user_id>/")
@login_required
def profile(user_id):
    my_profile = user_id is None
    user_id = user_id or int(current_user.get_id())
    user_profile = UserProfile.get(user_id)
    return render_template("carpool/profile.html", user_profile=user_profile,
                           my_profile=my_profile)


@carpool.route("/profile/edit/", methods=["GET", "POST"])
@login_required
def edit_profile():
    user_id = current_user.get_id()
    if request.method == "GET":
        user_profile = UserProfile.get(user_id)
        user_profile_form = _get_filled_user_profile_form(user_profile)
        if user_profile_form:
            return render_template("carpool/edit_profile.html", user_profile=user_profile,
                                   user_profile_form=user_profile_form, my_profile=True)
        else:
            flash("There were errors fetching your profile!")
            return redirect(url_for("carpool.index"))
    else:  # POST
        user_profile_form = UserProfileForm()
        if user_profile_form.validate_on_submit():
            _save_user_profile_form(user_profile_form, user_id)
            flash("Your profile was updated")
        else:
            flash("There were errors with your submission")
        return redirect(url_for("carpool.index"))


@carpool.route("/message/new/<int:receiver_id>/", methods=["GET", "POST"],
               defaults={"thread_id": None})
@carpool.route("/message/new/<int:receiver_id>/<int:thread_id>/", methods=["GET", "POST"])
@login_required
def new_message(receiver_id, thread_id):
    sender_id = int(current_user.get_id())
    message_form = MessageForm()
    if sender_id != receiver_id:  # not trying to mail yourself
        receiver = User.get(receiver_id)
        parent_message = None
        if receiver:  # valid receiver
            if thread_id: # check thread_id
                parent_message = Message.query.filter_by(id=thread_id).first()
                if parent_message is None:  # null thread_id if message is None
                    thread_id = None
                else:
                    if parent_message.thread_id is not None:  # Only accept root as parent id
                        thread_id = None
                        parent_message = None
            if request.method == "GET":
                return render_template("carpool/new_message.html",
                                       message_form=message_form,
                                       receiver=receiver,
                                       parent_message=parent_message)
            else:  # POST
                if message_form.validate_on_submit():
                    _store_message_form(message_form, sender_id, receiver_id, thread_id)
                    _inform_new_message(message_form, current_user, receiver)
                    return redirect(url_for("carpool.index"))
                else:
                    error_message = "There were errors in your submission!"
        else:
            error_message = "The message addressee is not found!"
    else:
        error_message = "Message can't be sent to one self"
        flash(error_message)
        return redirect(url_for("carpool.index"))
    flash(error_message)
    return render_template("carpool/new_message.html", message_form=message_form,
                           receiver=receiver)


@carpool.route("/inbox/", methods=["GET"], defaults={'inbox_type': None})
@carpool.route("/inbox/<inbox_type>/", methods=["GET"])
@login_required
def inbox(inbox_type):
    user_id = int(current_user.get_id())
    if inbox_type is None: # received messages
        messages = Message.query.filter_by(receiver_id=user_id).order_by(desc(Message.datetime)).limit(10).all()
    elif inbox_type =='sent':
        messages = Message.query.filter_by(sender_id=user_id).order_by(desc(Message.datetime)).limit(10).all()

    return render_template("carpool/inbox.html", messages=messages)


@carpool.route('/message/view/<message_id>/')
@login_required
def view_message(message_id):
    user_id = int(current_user.get_id())
    message = Message.query.filter(Message.id==message_id,
                                      or_(Message.sender_id==user_id,
                                          Message.receiver_id==user_id)
                                      ).first()
    if message and (message.read_status == 0):
        try:
            message.read_status = 1
            db.session.add(message)
            db.session.commit()
        except Exception as e:
            _logger.exception(str(e))
    return render_template("carpool/view_message.html", message=message)

@carpool.route("/tou/")
def terms_of_use():
    md = markdown.Markdown()
    f = open("app/templates/terms_of_use.md")
    text = f.read()
    f.close()
    content = md.convert(text)
    return render_template("carpool/terms_of_use.html", content=content)
