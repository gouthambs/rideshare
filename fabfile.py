__author__ = 'Gouthaman Balaraman'
from fabric.api import local, run, put, env, cd



def build():
    local("python setup.py sdist --formats=gztar")
    local("tar cvf dist/others.tar rideshare_nginx.conf rideshare_uwsgi.ini "
          "production.cfg main.py rideshare_supervisord.conf manage.py taskrunner.py"
          "")

def pushtest():
    # figure out the release name and version
    env.host_string = '55a848c95973ca1715000023@ucarpoolweb-ucarpool.rhcloud.com'

    dist = local('python setup.py --fullname', capture=True).strip()
    # upload the source tarball to the temporary folder on the server
    put('dist/%s.tar.gz' % dist, '/tmp/%s.tar.gz' % dist)
    put('dist/others.tar', '/tmp/others.tar')
    # create a place where we can unzip the tarball, then enter
    # that directory and unzip it
    run ('rm -rf /tmp/package')
    run('mkdir /tmp/package')
    run('tar xzf /tmp/%s.tar.gz -C /tmp/package' % dist)
    run('tar xvf /tmp/others.tar -C /tmp/package')

    with cd("/tmp/package/%s" % dist):
        run("python setup.py install")
        run('echo "SQLALCHEMY_DATABASE_URI = \'mysql+mysqldb://$OPENSHIFT_MYSQL_DB_USERNAME:$OPENSHIFT_MYSQL_DB_PASSWORD@$OPENSHIFT_MYSQL_DB_HOST:$OPENSHIFT_MYSQL_DB_PORT/ucarpoolweb\'" >> /tmp/package/production.cfg')
        run('echo "DEBUG = True" >> /tmp/package/production.cfg')
        run("python manage.py ../production.cfg db upgrade")
        # now setup the package with our virtual environment's
        # python interpreter
        run('cp -r static ${OPENSHIFT_REPO_DIR}/wsgi')

        run('cp -r /tmp/package/production.cfg ${OPENSHIFT_REPO_DIR}/')
        run('cp -r /tmp/package/manage.py ${OPENSHIFT_REPO_DIR}/')
        run("touch ${OPENSHIFT_REPO_DIR}/wsgi.py")


def tunnel(local_port=3307):
    remote_port = '3306'
    local("ssh -L 127.0.0.1:%s:127.0.0.1:%s gbalaraman@gblinode -N"%(str(local_port), str(remote_port)))


def pushprod():

    env.user = 'gbalaraman'
    env.host_string = "gblinode"


    appdir = "/var/www/rideshare"
    pyenv = appdir+"/env/bin"

    # figure out the release name and version

    dist = local('python setup.py --fullname', capture=True).strip()
    # upload the source tarball to the temporary folder on the server
    put('dist/%s.tar.gz' % dist, '/tmp/%s.tar.gz' % dist)

    put('dist/others.tar', '/tmp/others.tar')
    # create a place where we can unzip the tarball, then enter
    # that directory and unzip it
    run('rm -rf /tmp/package')
    run('mkdir /tmp/package')
    run('tar xzf /tmp/%s.tar.gz -C /tmp/package' % dist)
    run('tar xvf /tmp/others.tar -C %s' % appdir)

    with cd("/tmp/package/%s" % dist):
        run("%s/python setup.py install" % pyenv)
        run("%s/pip install uwsgi" % pyenv)
        run('echo "SQLALCHEMY_DATABASE_URI = \'mysql+mysqldb://webuser:webuser@localhost/rideshare\'" >> %s/production.cfg' % appdir)
        run("%s/python manage.py %s/production.cfg db upgrade" % (pyenv, appdir))

        # python interpreter
        run('cp -r static %s' % appdir)
        run('sudo supervisorctl restart uwsgi '
            '&& sudo supervisorctl restart rq')
    ##with cd("%s" % appdir):
    #    run('sudo supervisord -c supervisord.conf')


