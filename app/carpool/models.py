from flask import current_app as app
from flask.ext.security import user_registered
import logging
import datetime
from ..persistence import get_db
from ..auth.models import User
from sqlalchemy import UniqueConstraint


db = get_db()
_logger = logging.getLogger("carpool")


local_areas = db.Table("local_areas",
                       db.Column("commute_id", db.Integer, db.ForeignKey("commute.id")),
                       db.Column("local_area_id", db.Integer, db.ForeignKey("local_area.id"))
                       )


class LocalArea(db.Model):
    __tablename__ = "local_area"
    __table_args__ = (UniqueConstraint('locality', 'state', name='_locality_state_uc'),)
    id = db.Column(db.Integer, primary_key=True)
    locality = db.Column(db.String(64), index=True, nullable=False)
    state = db.Column(db.String(64), index=True, nullable=False)


class Commute(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    start_address = db.Column(db.String(255))
    end_address = db.Column(db.String(255))
    start_latitude = db.Column(db.Float, index=True)
    start_longitude = db.Column(db.Float, index=True)
    end_latitude = db.Column(db.Float, index=True)
    end_longitude = db.Column(db.Float, index=True)
    commute_days = db.Column(db.Integer, index=True)  # use 8 bits to code 0SSFTWTM
    start_time = db.Column(db.Time, index=True)
    end_time = db.Column(db.Time, index=True)
    user_id = db.Column(db.Integer,
                        db.ForeignKey("user.id", onupdate="CASCADE", ondelete="CASCADE"),
                        index=True)
    create_date = db.Column(db.DateTime)
    last_modified_date = db.Column(db.DateTime)
    share_type = db.Column(db.Integer)
    local_area = db.relationship('LocalArea', secondary=local_areas,
                                 backref=db.backref('commutes', lazy="dynamic"))
    comments = db.Column(db.String(256))

    def __str__(self):
        return self.start_address + " - "+self.end_address


class UserProfile(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer,
                        db.ForeignKey("user.id", onupdate="CASCADE", ondelete="CASCADE"),
                        index=True,
                        unique=True)
    description = db.Column(db.String(512))

    @property
    def user(self):
        if not hasattr(self, "_user"):
            if self.user_id:
                self._user = User.get(self.user_id)
        return self._user

    @classmethod
    def get(cls, user_id):
        return cls.query.filter_by(user_id=user_id).first()


class Message(db.Model):
    """
    Here thread_id acts as a pointer to root message. Lets say message #1 is the
    initiation of the thread. A reply to message #1, lets call it message #2, will
    have thread_id message #1. A reply to message #2, say message #3, will also have
    thread_id message #1. We just grab all with same thread id.
    """
    __tablename__ = "message"

    id = db.Column(db.Integer, primary_key=True)
    subject = db.Column(db.String(64))
    body = db.Column(db.String(512))
    sender_id = db.Column(db.Integer,
                          db.ForeignKey("user.id", onupdate="CASCADE", ondelete="CASCADE"),
                          index=True, nullable=False)
    receiver_id = db.Column(db.Integer,
                            db.ForeignKey("user.id", onupdate="CASCADE", ondelete="CASCADE"),
                            index=True, nullable=False)
    datetime = db.Column(db.DateTime, nullable=False)
    thread_id = db.Column(db.Integer, db.ForeignKey("message.id", onupdate="CASCADE", ondelete="CASCADE"))  # this should point to the master thread id
    children = db.relationship("Message", backref=db.backref("parent", remote_side=[id]))
    read_status = db.Column(db.SmallInteger, nullable=False, default=0, server_default='0')

    def sender(self):
        return User.get(self.sender_id)

    def receiver(self):
        return User.get(self.receiver_id)

    def thread_identifier(self):
        return self.thread_id or self.id

    def reply_to_identifier(self, current_user):
        return self.sender_id if self.sender_id != int(current_user.get_id()) else self.receiver_id

#
# class CommuteSearch(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     start_address = db.Column(db.String(255))
#     end_address = db.Column(db.String(255))
#     start_latitude = db.Column(db.Float, index=True)
#     start_longitude = db.Column(db.Float, index=True)
#     end_latitude = db.Column(db.Float, index=True)
#     end_longitude = db.Column(db.Float, index=True)
#     user_ip = db.Column(db.String(32))

def on_user_registered(context, user, confirm_token):
    try:
        profile = UserProfile()
        profile.user_id = user.get_id()
        #admin_user = app.config.get("ADMIN_USER")
        #if admin_user:
        #    body = "Thank you for signing up with us. " \
        #           "Please feel free to reach us if you have any questions " \
        #           "or suggestions for us. Hope you find your commute nirvana! " \
        #           "- UCarPool"
        #    message = Message(subject="Welcome", body=body,
        #                      sender_id=admin_user.id,
        #                      receiver_id=user.id,
        #                      datetime=datetime.datetime.utcnow())
        #    db.session.add(message)
        db.session.add(profile)
        db.session.commit()
    except Exception as e:
        _logger.exception(str(e))

