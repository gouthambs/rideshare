from ..persistence import get_db
from flask.ext.security import RoleMixin, UserMixin
import hashlib
from flask import current_app
import pytz


db = get_db()

# Define models
roles_users = db.Table('roles_users',
    db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
    db.Column('role_id', db.Integer(), db.ForeignKey('role.id'))
)


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __init__(self, name, description):
        self.name = name
        self.description = description

    def __str__(self):
        return self.nam

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    name = db.Column(db.String(255))
    gender = db.Column(db.String(2))  # M, F, O
    password = db.Column(db.String(255))
    active = db.Column(db.SmallInteger())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))
    last_login_at = db.Column(db.DateTime())
    current_login_at = db.Column(db.DateTime())
    last_login_ip = db.Column(db.String(32))
    current_login_ip = db.Column(db.String(32))
    login_count = db.Column(db.Integer())
    timezone = db.Column(db.String(64))

    _genders = {"M": "Male", "F": "Female", "O": "Other"}

    def get_tz(self):
        if self.timezone:
            return pytz.timezone(self.timezone)
        else:
            return current_app.config.get("DEFAULT_TIMEZONE", pytz.utc)

    def __str__(self):
        if self.name:
            return self.name
        else:
            return "User #%d" % self.id

    @classmethod
    def get(cls, user_id):
        user = None
        try:
            user = cls.query.filter_by(id=user_id).first()
        except Exception as e:
            user = None
        return user

    def gravatar_url(self, size=200):
        if self.email:
            hash = hashlib.md5(self.email.lower()).hexdigest()
            gravatar_url = "https://gravatar.com/avatar/%s?s=%d" % (hash, size)
        else:
            gravatar_url = "http://gravatar.com/avatar/00000000000000000000000000000000?d=mm&f=y&s=200"
        return  gravatar_url

    def get_gender(self):
        return self._genders[self.gender]
