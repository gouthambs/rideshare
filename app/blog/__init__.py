"""
The blogging engine
"""


def init_blogging(current_app, db, cache):
    from flask.ext.blogging import BloggingEngine, SQLAStorage

    from app.auth.models import User

    sql_storage = SQLAStorage(db=db, table_prefix="blog_")
    blog_engine = BloggingEngine(app=current_app, storage=sql_storage,
                                 cache=cache)

    @blog_engine.user_loader
    def load_user(user_id):
        return User.get(user_id)

    return blog_engine


