import os
import sys
from flask.ext.script import Manager, Command, Option
from flask.ext.migrate import Migrate, MigrateCommand
from app import create_app
from app.persistence import get_db


if len(sys.argv) < 2:
    print "Usage: manage.py <config_file> <migrate command>"
    exit(1)


BASE_DIR = os.path.dirname(__file__)
config_file = os.path.abspath(sys.argv[1])
app = create_app(config_file)
sys.argv.pop(0)
db = get_db()

class ModUserCommand(Command):
    option_list = (
        Option('--email', '-e', dest='email'),
        Option('--role', '-r', dest='role'),

    )

    def run(self, email, role):
        from app.auth.models import User, Role
        with app.app_context():
            user = User.query.filter_by(email=email).first()
            role_obj = Role.query.filter_by(name=role).first()
            if role_obj is None:
                role_obj = Role(role, role)
            if user is not None:
                if role_obj not in user.roles:
                    user.roles.append(role_obj)
                    db.session.add(role_obj)
                    db.session.add(user)
                    db.session.commit()
                    print "%s role added to user with email %s" % (role, email)
                else:
                    print "%s is already part of role %s" % (email, role)
            else:
                print "%s not found!" % email


class DelUserCommand(Command):
    option_list = (
        Option('--email', '-e', dest='email'),

    )

    def run(self, email):
        from app.auth.models import User, Role
        from app.carpool.models import Commute, Message, UserProfile
        from sqlalchemy import or_
        with app.app_context():
            user = User.query.filter_by(email=email).first()
            if user is not None:
                commutes = Commute.query.filter_by(user_id=user.id).all()
                for c in commutes:
                    db.session.delete(c)
                messages = Message.query.filter(or_(Message.sender_id==user.id, Message.receiver_id==user.id)).all()
                for m in messages:
                    db.session.delete(m)
                profile = UserProfile.query.filter_by(user_id=user.id).first()
                if profile:
                    db.session.delete(profile)
                db.session.delete(user)
                db.session.commit()
                print "User with email %s deleted" % (email)
            else:
                print "User with email %s not found" % (email)


if __name__ == '__main__':
    migrate = Migrate(app, db)
    manager = Manager(app)
    manager.add_command('db', MigrateCommand)
    manager.add_command('moduser', ModUserCommand)
    manager.add_command('deluser', DelUserCommand)
    manager.run()