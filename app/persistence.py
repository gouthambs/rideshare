from flask.ext.sqlalchemy import SQLAlchemy


_db = None
_cache = None

def init_sqlalchemy(app):
    global _db
    if _db is None:
        _db = SQLAlchemy(app)
    else:
        pass

def get_db():
    return _db


def init_cache(app):
    global _cache
    from flask.ext.cache import Cache
    if _cache is None:
        _cache = Cache(app)
    return _cache


def get_cache():
    return _cache