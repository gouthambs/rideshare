import os
from flask import Flask, render_template

__author__ = 'Gouthaman Balaraman'
__version__ = '0.6.7'


def create_app(config_filename=None, config_dict=None, init_models=True,
               init_extensions=True, init_signals=True, init_routes=True):
    APP_DIR = os.path.abspath(os.path.dirname(__file__))
    BASEDIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
    TEMPLATE_FOLDER = os.path.join(APP_DIR, "templates")
    STATIC_FOLDER = os.path.join(os.path.dirname(config_filename), "static")

    app = Flask(__name__,
                template_folder=TEMPLATE_FOLDER,
                static_folder=STATIC_FOLDER,
                static_url_path="/static")
    if config_filename:
        app.config.from_pyfile(config_filename)
    elif config_dict:
        app.config.update(config_dict)

    from .persistence import init_sqlalchemy, get_db
    init_sqlalchemy(app)
    db = get_db()

    if init_models:
        _init_app_models(app)

    if init_extensions:
        _init_extensions(app, db)

    if init_signals:
        _init_signals(app)

    if init_routes:
        _set_app_routes(app)

    return app


def _init_extensions(app, db):
    with app.app_context():
        from .persistence import init_cache
        cache = init_cache(app)

        from .auth import init_security
        init_security(app, db)

        from .blog import init_blogging
        init_blogging(app, db, cache)

        from flask.ext.mail import Mail
        mail = Mail(app)

        # the tasks depend on mail and security
        if app.config.get("TASK_QUEUE", False):
            from tasks import init_task_queue
            init_task_queue(app)

        from admin import init_admin
        init_admin(app, db)



def _init_signals(app):
    from carpool.models import on_user_registered
    from flask.ext.security import user_registered
    user_registered.connect(on_user_registered, app)


def _init_app_models(app):
    with app.app_context():
        from .carpool.models import Commute, UserProfile, Message
        from .auth.models import User, Role
        from .models import CommuteSearch


def _set_app_routes(app):

    # Sample HTTP error handling
    @app.errorhandler(404)
    def not_found(error):
        return render_template('404.html'), 404

    @app.before_first_request
    def map_admin_id(*args, **kwargs):
        from app.auth.models import User
        admin_email = app.config.get("ADMIN_EMAIL")
        if admin_email:
            app.config["ADMIN_USER"] = User.query.filter_by(email=admin_email.lower()).first()
        else:
            app.config["ADMIN_USER"] = None


    # add blueprints here
    from views import main
    app.register_blueprint(main)

    from carpool.views import carpool
    app.register_blueprint(carpool)
    return app


